#!/bin/bash

set -euo pipefail

DHPATH="${1}"

openssl dhparam -out "${DHPATH}"/dhparam.pem 4096
