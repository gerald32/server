#!/bin/bash

set -euo pipefail

HOSTNAME="$(grep HOSTNAME ./config.env | cut -d'=' -f2 | tr -d '\r')"
INVENTORY_SERVER="$(grep INVENTORY_SERVER ./config.env | cut -d'=' -f2 | tr -d '\r')"
AUTH_SERVER="$(grep AUTH_SERVER ./config.env | cut -d'=' -f2 | tr -d '\r')"
INFLUXDB_SERVER="$(grep INFLUXDB_SERVER ./config.env | cut -d'=' -f2 | tr -d '\r')"
QUEUE_SERVER="$(grep QUEUE_SERVER ./config.env | cut -d'=' -f2 | tr -d '\r')"
TASKS_SERVER="$(grep TASKS_SERVER ./config.env | cut -d'=' -f2 | tr -d '\r')"

for conf in nginx/conf.d/*.conf
do
  sed \
    -e "s/INVENTORY_SERVER/${INVENTORY_SERVER}/g" \
    -e "s/AUTH_SERVER/${AUTH_SERVER}/g" \
    -e "s/INFLUXDB_SERVER/${INFLUXDB_SERVER}/g" \
    -e "s/QUEUE_SERVER/${QUEUE_SERVER}/g" \
    -e "s/TASKS_SERVER/${TASKS_SERVER}/g" \
    -e "s/HOSTNAME/${HOSTNAME}/g" \
    ./"${conf}" > /etc/"${conf}"
done



