#!/bin/bash

set -euo pipefail

mkdir -p /opt/python
VERSION=3.10.4
INSTALL_TO=/opt/python

sudo apt install -y \
  build-essential checkinstall \
  libsqlite3-dev libc6-dev \
  libbz2-dev libssl-dev \
  libffi-dev zlib1g-dev \
  libreadline-dev

curl https://www.python.org/ftp/python/${VERSION}/Python-${VERSION}.tar.xz --output python.tar.xz
tar -xf python.tar.xz
cd Python-${VERSION}/
./configure LDFLAGS=-Wl,-rpath=${INSTALL_TO}/lib \
  --prefix=${INSTALL_TO} \
  --enable-shared \
  --enable-optimizations \
  --with-zlib \
  --with-readline
  # missing: readline, _uuid, _lzma
make
sudo make altinstall

