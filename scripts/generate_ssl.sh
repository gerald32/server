#!/bin/bash

set -euo pipefail

HOSTNAME="$(grep HOSTNAME ./config.env | cut -d'=' -f2 | tr -d '\r')"

openssl req -x509 \
  -newkey rsa:4096 \
  -keyout /etc/nginx/ssl/${HOSTNAME}.key \
  -out /etc/nginx/ssl/${HOSTNAME}.crt -sha256 -days 365 \
  -nodes