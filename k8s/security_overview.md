# SECURITY OVERVIEW

## KB MASTER
- lock down SSH and API access
- API local login only

## KB NODES
- accept only connections from control plane/port<br>
  configure via ansible, update/use network_values. yml
- accept connections for nodePort and LoadBalancer
- do not connect to public internet, *period*

## ETCD
- access by control plane only
- add TLS if possible; [docs](https://github.com/etcd-io/etcd/tree/main/Documentation)
- encrypt at rest

## WORKLOAD CONCERNS
- [RBAC Authorization](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)
- [Authentication](https://kubernetes.io/docs/concepts/security/controlling-access/)
- [Application Secrets](https://kubernetes.io/docs/concepts/configuration/secret/)
- [Encryption at Rest](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/)
- [Quality of Service](https://kubernetes.io/docs/tasks/configure-pod-container/quality-service-pod/)
- [Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
- [TLS Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/#tls)

## CONTAINER CONCERNS
- OS/dependency scans for vulnerabilities; build step
- Image Signing; build step


## ADDITIONAL CLUSTER SERVICES
- ?? mTLS for encryption across the cluster


## USER AUTHENTICATION
- client cert request<br>
  - /CN=\<username>
  - /O=\<group1>
  - /O=\<group2>
- sign with authority cert
- ?? k8s uses authority public key

## SERVICE ACCOUNTS
- --service-account-key-file: PEM encoded key for signing. uses server's TLS private key if not provided.
- --service-account-lookup: enable to revoke deleted tokens

## THIRD PARTY AUTHORIZATION
- [OpenID Connect](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#configuring-the-api-server)
- use `kubectl config` to load OIDC creds for a user; handles auto-renew


## NETWORK POLICY REQUIREMENTS

### General SaaS
- internal pod <> internal pod
- internal pod <> service
- service <> worker pod

### Longhorn Volumes
- namespace: longhorn-system
- pod: share-manager-\<volume-name>
- service: ???
- policies
  - app pod <> csi-plugin? 


