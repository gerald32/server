# TODO

### Ingress Controller
- nginx
- what configs are needed for microservices?


### Storage
- Storage Class:
  - provide a *type* of storage, such as "fast" or "cache"
- Persistent Volume:
  - the volume resource
  - must be static
  - create several
- Persistent Volume Claim:
  - the request to use the resource (mount)